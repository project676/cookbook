import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'formedit.dart';

class CRUDFoods extends StatefulWidget {
  @override
  _CRUDFoodsState createState() => _CRUDFoodsState();
}

class _CRUDFoodsState extends State<CRUDFoods> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 600,
      width: double.infinity,
      child: Column(
        children: <Widget>[
          Expanded(
            child: editfood(),
          )
        ],
      ),
    );
  }
}

class editfood extends StatefulWidget {
  editfood({Key? key}) : super(key: key);

  @override
  _editfoodState createState() => _editfoodState();
}

class _editfoodState extends State<editfood> {
  final Stream<QuerySnapshot> _foodsStream =
      FirebaseFirestore.instance.collection('foods').snapshots();

  CollectionReference foods = FirebaseFirestore.instance.collection('foods');

  Future<void> delfood(foodsId) {
    return foods
        .doc(foodsId)
        .delete()
        .then((value) => print('food Delete'))
        .catchError((error) => print('Failed to delete food: $error'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amber.shade700,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Color(0xFF3a3737),
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
        // brightness: Brightness.light,
        actions: [],
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: _foodsStream,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else
            return ListView(
              children: snapshot.data!.docs.map((doc) {
                return Card(
                  child: new InkWell(
                    child: Container(
                      height: 80,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20)),
                      child: Row(
                        children: [
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.only(
                                  top: 10, left: 10, right: 10, bottom: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        doc['name'],
                                        style: GoogleFonts.prompt(
                                            textStyle: TextStyle(
                                                color: Colors.black,
                                                fontSize: 20,
                                                fontWeight: FontWeight.w400,
                                                letterSpacing: .8)),
                                      ),
                                      Row(
                                        children: [
                                          IconButton(
                                              onPressed: () {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            formedit(
                                                                foodsId:
                                                                    doc.id)));
                                              },
                                              icon: Icon(
                                                Icons.create_sharp,
                                                size: 28,
                                              )),
                                          IconButton(
                                              onPressed: () async {
                                                await delfood(doc.id);
                                              },
                                              icon: Icon(
                                                Icons.delete_forever_sharp,
                                                size: 28,
                                              )),
                                        ],
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                );
              }).toList(),
            );
        },
      ),
    );
  }
}
