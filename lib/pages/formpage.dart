import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class formfood extends StatefulWidget {
  formfood({Key? key}) : super(key: key);

  @override
  _formfoodState createState() => _formfoodState();
}

class _formfoodState extends State<formfood> {
  String namefood = ' ';
  String about = '';
  String ingredient = '';
  CollectionReference foods = FirebaseFirestore.instance.collection('foods');

  Future<void> addfood() {
    return foods
        .add({
          'name': namefood,
          'about': about,
          'ingredient': ingredient,
        })
        .then((value) => print('foods added'))
        .catchError((error) => print('Failed to add foods: $error'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amber.shade700,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Color(0xFF3a3737),
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
        // brightness: Brightness.light,
        actions: [],

        // actions: <Widget>[
        //   IconButton(
        //       icon: Icon(
        //         Icons.business_center,
        //         color: Color(0xFF3a3737),
        //       ),
        //       onPressed: () {
        //         Navigator.push(context, ScaleRoute(page: FoodOrderPage()));
        //       })
        // ],
      ),
      body: Container(
        padding: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 0),
        width: double.infinity,
        height: double.infinity,
        color: Colors.grey.shade300,
        child: Column(
          children: <Widget>[
            Flexible(
              flex: 30,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                      width: 100,
                      height: 100,
                      alignment: Alignment.center,
                      child: Column(
                        children: [
                          Icon(
                            Icons.local_restaurant_sharp,
                            size: 40,
                            color: Colors.amber.shade700,
                          ),
                          Text('CooK Book')
                        ],
                      )),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: <Widget>[
                      Flexible(
                          child: TextField(
                        onChanged: (value) {
                          setState(() {
                            namefood = value;
                          });
                        },
                        showCursor: true,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                            borderSide: BorderSide(
                              width: 0,
                              style: BorderStyle.none,
                            ),
                          ),
                          filled: true,
                          fillColor: Color(0xFFF2F3F5),
                          hintStyle: TextStyle(
                            color: Color(0xFF666666),
                          ),
                          hintText: "ชื่อเมนู",
                        ),
                      ))
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: <Widget>[
                      Flexible(
                          child: TextField(
                        keyboardType: TextInputType.multiline,
                        maxLines: 2,
                        onChanged: (value) {
                          setState(() {
                            ingredient = value;
                          });
                        },
                        showCursor: true,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                            borderSide: BorderSide(
                              width: 0,
                              style: BorderStyle.none,
                            ),
                          ),
                          filled: true,
                          fillColor: Color(0xFFF2F3F5),
                          hintStyle: TextStyle(
                            color: Color(0xFF666666),
                          ),
                          hintText: "ส่วนผสม",
                        ),
                      ))
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: <Widget>[
                      Flexible(
                          child: TextField(
                        keyboardType: TextInputType.multiline,
                        maxLines: 2,
                        onChanged: (value) {
                          setState(() {
                            about = value;
                          });
                        },
                        showCursor: true,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                            borderSide: BorderSide(
                              width: 0,
                              style: BorderStyle.none,
                            ),
                          ),
                          filled: true,
                          fillColor: Color(0xFFF2F3F5),
                          hintStyle: TextStyle(
                            color: Color(0xFF666666),
                          ),
                          hintText: "วิธีทำ",
                        ),
                      ))
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    width: 250,
                    height: 40,
                    decoration: new BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5.0)),
                        color: Colors.amber),
                    child: MaterialButton(
                      // splashColor: Color(0xFF118531),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 1.0, horizontal: 1.0),
                        child: Text(
                          "บันทึกสูตรอาหาร",
                          style: GoogleFonts.prompt(
                              textStyle: TextStyle(
                                  color: Colors.black,
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  letterSpacing: .8)),
                        ),
                      ),
                      onPressed: () async {
                        addfood();
                        Navigator.pop(context);
                      },
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
