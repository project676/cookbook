import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class formedit extends StatefulWidget {
  String foodsId;
  formedit({Key? key, required this.foodsId}) : super(key: key);

  @override
  _formeditState createState() => _formeditState(this.foodsId);
}

class _formeditState extends State<formedit> {
  String foodsId;
  String namefood = '';
  String about = '';
  String ingredient = '';
  CollectionReference foods = FirebaseFirestore.instance.collection('foods');
  _formeditState(this.foodsId);
  TextEditingController _namefood = new TextEditingController();
  TextEditingController _about = new TextEditingController();
  TextEditingController _ingredient = new TextEditingController();

  @override
  void initState() {
    super.initState();
    if (this.foodsId.isNotEmpty) {
      foods.doc(this.foodsId).get().then((snapshot) {
        if (snapshot.exists) {
          var data = snapshot.data() as Map<String, dynamic>;
          setState(() {
            namefood = data['name'];
            about = data['about'];
            ingredient = data['ingredient'];
            _namefood.text = namefood;
            _about.text = about;
            _ingredient.text = ingredient;
          });
        }
      });
    }
  }

  Future<void> updateFood() {
    return foods
        .doc(this.foodsId)
        .update({
          'name': this.namefood,
          'about': this.about,
          'ingredient': this.ingredient,
        })
        .then((value) => print("food Updated"))
        .catchError((error) => print("Failed to update food: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amber.shade700,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Color(0xFF3a3737),
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
        // brightness: Brightness.light,
        actions: [],

        // actions: <Widget>[
        //   IconButton(
        //       icon: Icon(
        //         Icons.business_center,
        //         color: Color(0xFF3a3737),
        //       ),
        //       onPressed: () {
        //         Navigator.push(context, ScaleRoute(page: FoodOrderPage()));
        //       })
        // ],
      ),
      body: Container(
        padding: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 0),
        width: double.infinity,
        height: double.infinity,
        color: Colors.grey.shade300,
        child: Column(
          children: <Widget>[
            Flexible(
              flex: 30,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                      width: 100,
                      height: 100,
                      alignment: Alignment.center,
                      child: Column(
                        children: [
                          Icon(
                            Icons.local_restaurant_sharp,
                            size: 40,
                            color: Colors.amber.shade700,
                          ),
                          Text('CooK Book')
                        ],
                      )),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: <Widget>[
                      Flexible(
                          child: TextField(
                        onChanged: (value) {
                          setState(() {
                            namefood = value;
                          });
                        },
                        controller: _namefood,
                        showCursor: true,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                            borderSide: BorderSide(
                              width: 0,
                              style: BorderStyle.none,
                            ),
                          ),
                          filled: true,
                          fillColor: Color(0xFFF2F3F5),
                          hintStyle: TextStyle(
                            color: Color(0xFF666666),
                          ),
                          hintText: "ชื่อเมนู",
                        ),
                      ))
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: <Widget>[
                      Flexible(
                          child: TextField(
                        keyboardType: TextInputType.multiline,
                        maxLines: 2,
                        onChanged: (value) {
                          setState(() {
                            ingredient = value;
                          });
                        },
                        controller: _ingredient,
                        showCursor: true,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                            borderSide: BorderSide(
                              width: 0,
                              style: BorderStyle.none,
                            ),
                          ),
                          filled: true,
                          fillColor: Color(0xFFF2F3F5),
                          hintStyle: TextStyle(
                            color: Color(0xFF666666),
                          ),
                          hintText: "ส่วนผสม",
                        ),
                      ))
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: <Widget>[
                      Flexible(
                          child: TextField(
                        keyboardType: TextInputType.multiline,
                        maxLines: 2,
                        onChanged: (value) {
                          setState(() {
                            about = value;
                          });
                        },
                        controller: _about,
                        showCursor: true,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                            borderSide: BorderSide(
                              width: 0,
                              style: BorderStyle.none,
                            ),
                          ),
                          filled: true,
                          fillColor: Color(0xFFF2F3F5),
                          hintStyle: TextStyle(
                            color: Color(0xFF666666),
                          ),
                          hintText: "วิธีทำ",
                        ),
                      ))
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    width: 250,
                    height: 40,
                    decoration: new BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5.0)),
                        color: Colors.amber),
                    child: MaterialButton(
                      // splashColor: Color(0xFF118531),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 1.0, horizontal: 1.0),
                        child: Text(
                          "บันทึกสูตรอาหาร",
                          style: GoogleFonts.prompt(
                              textStyle: TextStyle(
                                  color: Colors.black,
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  letterSpacing: .8)),
                        ),
                      ),
                      onPressed: () async {
                        updateFood();
                        Navigator.pop(context);
                      },
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
