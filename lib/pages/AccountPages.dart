import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'CRUD.dart';
import 'formpage.dart';

class AccountPage extends StatefulWidget {
  AccountPage({Key? key}) : super(key: key);

  @override
  _AccountPageState createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  final Stream<QuerySnapshot> _userStream = FirebaseFirestore.instance
      .collection('users')
      // .orderBy('name', descending: false)
      .snapshots();

  CollectionReference users = FirebaseFirestore.instance.collection('users');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // appBar: AppBar(
        //   backgroundColor: Color(0xFFFAFAFA),
        //   elevation: 0,
        //   centerTitle: true,
        //   title: Text(
        //     'Profile',
        //     style: GoogleFonts.prompt(
        //         textStyle: TextStyle(color: Colors.black, fontSize: 20)),
        //   ),

        // leading: IconButton(
        //   icon: Icon(
        //     Icons.arrow_back_ios,
        //     color: Color(0xFF3a3737),
        //   ),
        //   onPressed: () => Navigator.of(context).pop(),
        // ),
        // brightness: Brightness.light,
        // actions: [],

        // actions: <Widget>[
        //   IconButton(
        //       icon: Icon(
        //         Icons.business_center,
        //         color: Color(0xFF3a3737),
        //       ),
        //       onPressed: () {
        //         Navigator.push(context, ScaleRoute(page: FoodOrderPage()));
        //       })
        // ],
        // ),
        body: StreamBuilder<QuerySnapshot>(
      stream: _userStream,
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else
          return ListView(
            children: snapshot.data!.docs.map((doc) {
              // return Text(doc['name']);
              return Container(
                padding: EdgeInsets.only(
                  top: 10,
                  right: 18,
                  left: 18,
                ),
                child: new InkWell(
                  child: Container(
                    height: 500,
                    decoration: BoxDecoration(
                        color: Colors.white10,
                        borderRadius: BorderRadius.circular(20)),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              width: 100.0,
                              height: 100.0,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  image: DecorationImage(
                                      fit: BoxFit.cover,
                                      image: NetworkImage(
                                          'https://firebasestorage.googleapis.com/v0/b/cookbook-bb297.appspot.com/o/imgtest%2Fpngegg%20(35)1.png?alt=media&token=d051e56d-ab49-4943-baeb-9638e3f26539'))),
                            ),
                            Expanded(
                              child: Container(
                                padding: EdgeInsets.only(
                                    top: 5, left: 10, right: 10, bottom: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          doc['name'],
                                          style: GoogleFonts.prompt(
                                              textStyle: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 20,
                                                  fontWeight: FontWeight.bold,
                                                  letterSpacing: .8)),
                                        ),
                                      ],
                                    ),
                                    Text(
                                      doc['email'],
                                      style: GoogleFonts.prompt(
                                          textStyle: TextStyle(
                                              color: Colors.black54,
                                              fontSize: 16,
                                              letterSpacing: .8)),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        Column(
                          children: [
                            Padding(
                                padding: EdgeInsets.only(top: 5, bottom: 5)),
                            Divider(
                              color: Colors.black26,
                              height: 2,
                              thickness: 1,
                              // indent: 50,
                              // endIndent: 5,
                            )
                          ],
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => formfood()));
                          },
                          child: Container(
                            padding:
                                EdgeInsets.only(top: 17, left: 10, right: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'เพิ่มเมนูอาหารของคุณ',
                                      style: GoogleFonts.prompt(
                                          textStyle: TextStyle(
                                              color: Colors.black,
                                              fontSize: 20,
                                              fontWeight: FontWeight.w400,
                                              letterSpacing: .8)),
                                    ),
                                    Icon(
                                      Icons.arrow_forward_ios_outlined,
                                      size: 15,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        Column(
                          children: [
                            Padding(
                                padding: EdgeInsets.only(top: 5, bottom: 5)),
                            Divider(
                              color: Colors.black26,
                              height: 2,
                              thickness: 1,
                              // indent: 50,
                              // endIndent: 5,
                            )
                          ],
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => CRUDFoods()));
                          },
                          child: Container(
                            padding:
                                EdgeInsets.only(top: 17, left: 10, right: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'รายการอาหารของคุณ',
                                      style: GoogleFonts.prompt(
                                          textStyle: TextStyle(
                                              color: Colors.black,
                                              fontSize: 20,
                                              fontWeight: FontWeight.w400,
                                              letterSpacing: .8)),
                                    ),
                                    Icon(
                                      Icons.arrow_forward_ios_outlined,
                                      size: 15,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        Column(
                          children: [
                            Padding(
                                padding: EdgeInsets.only(top: 5, bottom: 5)),
                            Divider(
                              color: Colors.black26,
                              height: 2,
                              thickness: 1,
                              // indent: 50,
                              // endIndent: 5,
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              );
            }).toList(),
          );
      },
    ));
//
  }
}
