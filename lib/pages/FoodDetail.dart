import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class FoodDeatil extends StatefulWidget {
  final String foodsId;
  FoodDeatil({Key? key, required this.foodsId}) : super(key: key);

  @override
  _FoodDeatilState createState() => _FoodDeatilState(this.foodsId);
}

class _FoodDeatilState extends State<FoodDeatil> {
  String foodsId = '';
  String name = '';
  String about = '';

  final Stream<QuerySnapshot> _foodStream =
      FirebaseFirestore.instance.collection('foods').snapshots();

  CollectionReference food = FirebaseFirestore.instance.collection('foods');
  _FoodDeatilState(this.foodsId);

  // TextEditingController _fulNameController = new TextEditingController();
  // TextEditingController _companyController = new TextEditingController();
  // TextEditingController _ageController = new TextEditingController();
  @override
  void initState() {
    super.initState();
    setState(() {
      this.foodsId = foodsId;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.amber.shade700,
          elevation: 0,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: Color(0xFF3a3737),
            ),
            onPressed: () => Navigator.of(context).pop(),
          ),
          // brightness: Brightness.light,
          actions: [],

          // actions: <Widget>[
          //   IconButton(
          //       icon: Icon(
          //         Icons.business_center,
          //         color: Color(0xFF3a3737),
          //       ),
          //       onPressed: () {
          //         Navigator.push(context, ScaleRoute(page: FoodOrderPage()));
          //       })
          // ],
        ),
        body: StreamBuilder<QuerySnapshot>(
          stream: FirebaseFirestore.instance
              .collection('foods')
              .where('name', isEqualTo: foodsId)
              .snapshots(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else
              return ListView(
                children: snapshot.data!.docs.map((doc) {
                  return Container(
                    padding: EdgeInsets.only(
                      top: 10,
                      right: 18,
                      left: 18,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              doc['name'],
                              textAlign: TextAlign.center,
                              style: GoogleFonts.prompt(
                                  textStyle: TextStyle(
                                color: Colors.black,
                                fontSize: 25,
                                fontWeight: FontWeight.bold,
                                letterSpacing: .8,
                              )),
                            ),
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 5, bottom: 5),
                          width: 400.0,
                          height: 200.0,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: NetworkImage(
                                      'https://firebasestorage.googleapis.com/v0/b/cookbook-bb297.appspot.com/o/imgtest%2Fimgfoods1.jpg?alt=media&token=770d87e6-006c-47ca-b6dc-a191588179ef'))),
                        ),
                        Text(
                          'ส่วนผสม : ',
                          style: GoogleFonts.prompt(
                              textStyle: TextStyle(
                                  color: Colors.black87,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: .8)),
                        ),
                        Text(
                          doc['ingredient'],
                          style: GoogleFonts.prompt(
                              textStyle: TextStyle(
                                  color: Colors.black87,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: .8)),
                        ),
                        Divider(
                          color: Colors.black12,
                          height: 1,
                          thickness: 1,
                          // indent: 50,
                          // endIndent: 5,
                        ),
                        Text(
                          'วิธีทำอาหาร : ',
                          style: GoogleFonts.prompt(
                              textStyle: TextStyle(
                                  color: Colors.black87,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: .8)),
                        ),
                        Text(
                          doc['about'],
                          style: GoogleFonts.prompt(
                              textStyle: TextStyle(
                                  color: Colors.black87,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: .8)),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(''),
                            IconButton(
                              icon: const Icon(Icons.favorite_sharp),
                              color: Colors.red,
                              iconSize: 30,
                              splashColor: Colors.purple,
                              onPressed: () {},
                              tooltip: "favorite",
                            )
                          ],
                        ),
                      ],
                    ),
                  );
                }).toList(),
              );
          },
        ));
  }
}
