import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:project_app_cookbook/pages/AccountPages.dart';
import 'package:project_app_cookbook/widgets/FoodWidget.dart';
import 'package:project_app_cookbook/widgets/favoriteWidget.dart';

class HomeBottomNavigationScreen extends StatefulWidget {
  @override
  _HomeBottomNavigationScreenState createState() =>
      _HomeBottomNavigationScreenState();
}

class _HomeBottomNavigationScreenState
    extends State<HomeBottomNavigationScreen> {
  final List<Widget> _children = [
    BestFoodWidget(),
    FavoriteWidget(),
    AccountPage()

    // CartScreen(),
    // AccountScreen(),
  ];

  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    final labelTextStyle =
        Theme.of(context).textTheme.subtitle2!.copyWith(fontSize: 8.0);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amber.shade700,
        elevation: 5,
        centerTitle: true,
        title: Text(
          "CookBook",
          // style: TextStyle(
          //     color: Color(0xFF3a3737),
          //     fontSize: 16,
          //     fontWeight: FontWeight.w500),
          style: GoogleFonts.openSans(
              textStyle: TextStyle(
                  color: Colors.black,
                  fontSize: 25,
                  fontWeight: FontWeight.bold)),
        ),
        // brightness: Brightness.light,
        actions: <Widget>[
          // IconButton(
          //     icon: Icon(
          //       Icons.notifications_none,
          //       color: Color(0xFF3a3737),
          //     ),
          //     onPressed: () {})
        ],
      ),
      body: _children[selectedIndex],
      bottomNavigationBar: SizedBox(
        height: 50.0,
        child: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          selectedItemColor: Colors.amber.shade700,
          unselectedItemColor: Colors.grey,
          currentIndex: selectedIndex,
          selectedLabelStyle: labelTextStyle,
          unselectedLabelStyle: labelTextStyle,
          onTap: (index) {
            setState(() {
              selectedIndex = index;
            });
          },
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'SWIGGY',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.favorite_sharp),
              label: 'favorite',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.account_circle_sharp),
              label: 'ACCOUNT',
            ),
          ],
        ),
      ),
    );
  }
}
