import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:project_app_cookbook/pages/FoodDetail.dart';

class FavoriteWidget extends StatefulWidget {
  @override
  _FavoriteWidgetState createState() => _FavoriteWidgetState();
}

class _FavoriteWidgetState extends State<FavoriteWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      child: Column(
        children: <Widget>[
          // BestFoodTitle(),
          Expanded(
            child: favoriteList(),
          )
        ],
      ),
    );
  }
}

class favoriteList extends StatefulWidget {
  favoriteList({Key? key}) : super(key: key);

  @override
  _favoriteListState createState() => _favoriteListState();
}

class _favoriteListState extends State<favoriteList> {
  final Stream<QuerySnapshot> _storeStream = FirebaseFirestore.instance
      .collection('foods')
      // .orderBy('name', descending: false)
      .snapshots();

  CollectionReference stores = FirebaseFirestore.instance.collection('foods');

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: _storeStream,
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else
          return ListView(
            children: snapshot.data!.docs.map((doc) {
              // return Text(doc['name']);
              return Card(
                child: new InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                FoodDeatil(foodsId: doc['name'])));
                  },
                  child: Container(
                    height: 110,
                    decoration: BoxDecoration(
                        color: Colors.white10,
                        borderRadius: BorderRadius.circular(20)),
                    child: Row(
                      children: [
                        Container(
                          width: 100.0,
                          height: 100.0,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15.0),
                              image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: NetworkImage(
                                      'https://firebasestorage.googleapis.com/v0/b/cookbook-bb297.appspot.com/o/imgtest%2Fimgfoods1.jpg?alt=media&token=770d87e6-006c-47ca-b6dc-a191588179ef'))),
                        ),
                        Expanded(
                          child: Container(
                            padding:
                                EdgeInsets.only(top: 17, left: 10, right: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      doc['name'],
                                      style: GoogleFonts.prompt(
                                          textStyle: TextStyle(
                                              color: Colors.black,
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold,
                                              letterSpacing: .8)),
                                    ),
                                    Icon(
                                      Icons.arrow_forward_ios_outlined,
                                      size: 15,
                                    ),
                                  ],
                                ),
                                // Text(
                                //   doc['about'],
                                //   style: GoogleFonts.prompt(
                                //       textStyle: TextStyle(
                                //           color: Colors.black54,
                                //           fontSize: 16,
                                //           letterSpacing: .8)),
                                // ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              );
            }).toList(),
          );
      },
    );
//       bottomNavigationBar: BottomAppBar(
//         shape: CircularNotchedRectangle(),
//         notchMargin: 8.0,
//         child: Row(
//           mainAxisSize: MainAxisSize.max,
//           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//           children: <Widget>[
//             SizedBox(width: 120.0),
//             IconButton(
//               icon: Icon(
//                 Icons.add_location_alt_outlined,
//               ),
//               onPressed: () {
//                 Navigator.push(context,
//                     MaterialPageRoute(builder: (context) => AdminHomepageWidget()));
//               },
//             )
//           ],
//         ),
//       ),
//       floatingActionButtonLocation: FloatingActionButtonLocation.startDocked,
//       floatingActionButton: FloatingActionButton(
//         backgroundColor: Colors.green[900],
//         child: Icon(Icons.map_outlined),
//         onPressed: () {
//           Navigator.push(
//               context, MaterialPageRoute(builder: (context) => MapWidget()));
//         },
//       ),
  }
}
